# GitLab + Mermaid evaluation thingy

## Graph Types

### flowchart LR

```mermaid
flowchart LR
    startNode[start here 🚀]
    intermediateNode[via here]
    stopNode[finish here 🏁]

    startNode --> intermediateNode
    intermediateNode --> stopNode
    intermediateNode --> intermediateNode

```

### flowchart TD

```mermaid
flowchart TD
    startNode[start here 🚀]
    intermediateNode[via here]
    stopNode[finish here 🏁]

    startNode --> intermediateNode
    intermediateNode --> stopNode
    intermediateNode --> intermediateNode
```

### Node Shapes

```mermaid
graph LR
    startNode[start here 🚀]
    intermediateNode[via here]
    stopNode[finish here 🏁]

    startNode --> intermediateNode
    intermediateNode --> stopNode
    intermediateNode --> intermediateNode

    startNode --> stopNode
    stopNode --> startNode

    startNode --> startNode
    stopNode --> stopNode
```
